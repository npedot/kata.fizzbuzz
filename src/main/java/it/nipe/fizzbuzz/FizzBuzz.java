package it.nipe.fizzbuzz;

/**
 * FizzBuzz Calculator
 *
 */
public class FizzBuzz
{

    public String runOn(int i) {
        String result = "";
        if (i%3==0) result +="fizz";
        if (i%5==0) result += "buzz";
        if ("".equals(result)) result = ""+i;
        return result;
    }
}
