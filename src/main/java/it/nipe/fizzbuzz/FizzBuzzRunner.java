package it.nipe.fizzbuzz;

/**
 * FizzBuzz on int range.
 */
public class FizzBuzzRunner {
    public String runOn(int lowerBound, int upperBound) {
        FizzBuzz fizzbuzz = new FizzBuzz();
        String result = "";
        for (int i= lowerBound; i<=upperBound; i++) {
            result += fizzbuzz.runOn(i);
            if (i!=upperBound) result += ", ";
        }
        return result;
    }
}
