package it.nipe.fizzbuzz;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Test FizzBuzz on a range.
 */
@RunWith(Parameterized.class)
public class FizzBuzzRunnerTest {

    private FizzBuzzRunner fizzbuzzRunner;
    private int lowerBound;
    private int upperBound;
    private String expectedResult;

    public FizzBuzzRunnerTest(int lowerBound, int upperBound, String expectedResult) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.expectedResult = expectedResult;
    }

    @Before
    public void each_test() {
        fizzbuzzRunner = new FizzBuzzRunner();
    }

    @Test
    public void fizzbuzzOnRange() {
        assertThat(fizzbuzzRunner.runOn(lowerBound,upperBound), is(expectedResult));
    }

    @Parameterized.Parameters
    public static List<Object[]> scenarios() {
        return Arrays.asList(new Object[][] {
                {1,3,"1, 2, fizz"},
                {1,5,"1, 2, fizz, 4, buzz"},
                {0,10,"fizzbuzz, 1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz"}
        });
    }
}
