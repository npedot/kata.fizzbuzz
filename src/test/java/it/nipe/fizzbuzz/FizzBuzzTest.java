package it.nipe.fizzbuzz;


import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Unit test for simple FizzBuzz.
 */
public class FizzBuzzTest
{
    private FizzBuzz fizzbuzz;

    @Before
    public void setUp() {
        fizzbuzz = new FizzBuzz();
    }

    @Test
    public void replaces_any_number_divisible_by_three_with_the_word_fizz()
    {
        assertThat(fizzbuzz.runOn(3),is("fizz"));
        assertThat(fizzbuzz.runOn(6),is("fizz"));
        assertThat(fizzbuzz.runOn(5),is(not("fizz")));
    }

    @Test
    public void replaces_any_number_divisible_by_five_with_the_word_buzz()
    {
        assertThat(fizzbuzz.runOn(5),is("buzz"));
        assertThat(fizzbuzz.runOn(10),is("buzz"));
        assertThat(fizzbuzz.runOn(7),is(not("buzz")));
    }

    @Test
    public void replaces_any_number_divisible_by_three_and_five_with_the_word_fizzbuzz() {
        assertThat(fizzbuzz.runOn(0),is("fizzbuzz"));
        assertThat(fizzbuzz.runOn(15),is("fizzbuzz"));
    }

    @Test
    public void not_replaces_any_other_numbe()
    {
        //assertThat(fizzbuzz.runOn(0),is("0"));
        assertThat(fizzbuzz.runOn(1),is("1"));
        assertThat(fizzbuzz.runOn(7),is("7"));
    }
}
